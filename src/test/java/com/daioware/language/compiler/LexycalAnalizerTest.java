package com.daioware.language.compiler;

import java.util.List;

import com.daioware.language.compiler.Symbol.Type;

public class LexycalAnalizerTest {

	protected static void validateSymbol(Symbol symbol,String text,Type type) {
		assertTrue(symbol.getText().equals(text),"Type:"+symbol.getType()+", Expected:"+text+", actual:"+symbol.getText());
		assertTrue(symbol.getType().equals(type),"Text:"+symbol.getText()+", Expected:"+type+", actual:"+symbol.getType());

	}
	protected static void assertTrue(boolean b,String text) {
		if(!b) {
			throw new AssertionError(text);
		}
	}
	
	public static void main(String[] args) throws InvalidSymbolException {
		String text="- http://localhost:80/diego/hello.txt identifier 49  \"my beautiful string =)\" "
				+ "46.464>=&&35||&|^?\"my other beauti\\\"ful string =)\"";
		LexicalAnalyzer lex=new LexicalAnalyzer(text);
		int pos=0;
		List<Symbol> symbols=lex.parse();
		/*for(Symbol s:symbols) {
			System.out.println(s);
		}*/
		validateSymbol(symbols.get(pos++), "-",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), "http",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), ":",Type.UNKNOWN);
		validateSymbol(symbols.get(pos++), "/",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "/",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "localhost",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), ":",Type.UNKNOWN);
		validateSymbol(symbols.get(pos++), "80",Type.INTEGER);
		validateSymbol(symbols.get(pos++), "/",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "diego",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), "/",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "hello",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), ".",Type.UNKNOWN);
		validateSymbol(symbols.get(pos++), "txt",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), "identifier",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), "49",Type.INTEGER);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), "my beautiful string =)",Type.STRING);
		validateSymbol(symbols.get(pos++), " ",Type.WHITE_SPACE);
		validateSymbol(symbols.get(pos++), "46.464",Type.FLOAT);

		validateSymbol(symbols.get(pos++), ">=",Type.RELATIONAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "&&",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "35",Type.INTEGER);
		validateSymbol(symbols.get(pos++), "||",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "&",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "|",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "^",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "?",Type.LOGICAL_OPERATOR);
		validateSymbol(symbols.get(pos++), "my other beauti\"ful string =)",Type.STRING);
		
		text="-file=\"diego\\my incr File.txt\"";
		lex.setText(text);
		symbols=lex.parse();
		pos=0;
		validateSymbol(symbols.get(pos++), "-",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "file",Type.IDENTIFIER);
		validateSymbol(symbols.get(pos++), "=",Type.ARITHMETICS_OPERATOR);
		validateSymbol(symbols.get(pos++), "diego\\my incr File.txt",Type.STRING);
		
		System.out.println("Success =)");

	}
}
