package com.daioware.language.compiler;

import java.util.Map;
import java.util.Map.Entry;

import com.daioware.language.compiler.parser.ParseException;
import com.daioware.language.compiler.parser.cmd.CommandArgumentsParser;

import junit.framework.TestCase;

public class CommandArgumentsParserTest2 extends TestCase{

	public static void main(String args[]) throws ParseException {
		CommandArgumentsParser parser=new CommandArgumentsParser();
		Map<String,String> argsMap=parser.parse(args);
		for(Entry<String, String> entry:argsMap.entrySet()) {
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
	}
}
