package com.daioware.language.compiler;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.daioware.language.compiler.parser.ParseException;
import com.daioware.language.compiler.parser.cmd.CommandArgumentsParser;

import junit.framework.TestCase;

public class CommandArgumentsParserTest extends TestCase{

	@Test
	public void test() throws ParseException {
		String source[]=new String[1];
		String file="C:\\Users\\Diego Olvera\\Documents\\dev\\java\\_maven-archetypes\\javaApp\\pom.xml";
		String port="180";
		String firstValueWithNoKey="firstValueWithoutKey",secondValue="secondValueWithoutKey";
		String test="";
		source[0]=" \"-diego 0\"-arg1 -arg2 -file=\""+file+"\" -test \"-diego\"-p "+port+" "+firstValueWithNoKey+" "+secondValue+" -otro=hello -test2"
				+" -url=https://localhost:80 -file2=\""+file+"\" \"-diego 2\"";
		CommandArgumentsParser parser=new CommandArgumentsParser();
		parser.setFlags(Arrays.asList("arg1","arg2","test","test2"));
		Map<String,String> args=parser.parse(source);
		String val;
		for(Entry<String, String> entry:args.entrySet()) {
			System.out.println(entry.getKey()+":"+entry.getValue());
		}
		assertTrue(args.containsKey("arg1"));
		assertTrue(args.containsKey("arg2"));
		assertTrue(args.containsKey("test"));
		assertTrue(args.containsKey("test2"));
		
		assertNotNull(val=args.get("file"));
		assertEquals(val, file);
		
		assertNotNull(val=args.get("file2"));
		assertEquals(val, file);
		
		assertNotNull(val=args.get("p"));
		assertEquals(val, port);
		
		assertNotNull(val=args.get("0"));
		assertEquals(val, "-diego 0");
		
		assertNotNull(val=args.get("1"));
		assertEquals(val, "-diego");
		
		assertNotNull(val=args.get("2"));
		assertEquals(val, firstValueWithNoKey);
		
		assertNotNull(val=args.get("3"));
		assertEquals(val, secondValue);
		
		assertNotNull(val=args.get("4"));
		assertEquals(val, "-diego 2");
		
		assertNotNull(val=args.get("test"));
		assertEquals(val, test);

		assertNotNull(val=args.get("otro"));
		assertEquals(val, "hello");
		
		assertNotNull(val=args.get("url"));
		assertEquals(val, "https://localhost:80");
		
		file="C:\\Users\\djolvera\\Documents\\dev\\java\\TextReplacement\\testFolder\\jdbc.zip\\jdbc";
		source[0]="-p \""+file+"\" "
				+ "-tr \"jdbc:oracle:thin:.*<\" -r \"jdbc:oracle:thin:@slcao718.us.oracle.com:1561:si3yd570<\"";
		
		args=parser.parse(source);
		
		assertNotNull(val=args.get("p"));
		assertEquals(val, file);
				
	}
}
