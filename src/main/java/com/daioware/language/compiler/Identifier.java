package com.daioware.language.compiler;

import java.util.function.Consumer;

public class Identifier extends Number{
	public Identifier(String text) {
		super(text);
	}	
	public boolean evaluate(Evaluator ev) throws InvalidEvaluation {
		return ev.isTrue(getText());
	}
	public void consumeVars(Consumer<Object> c) {
		c.accept(getText());
	}
}
