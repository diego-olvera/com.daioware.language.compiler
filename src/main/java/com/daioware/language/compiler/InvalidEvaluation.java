package com.daioware.language.compiler;

public class InvalidEvaluation extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidEvaluation(String message) {
		super(message);
	}

}
