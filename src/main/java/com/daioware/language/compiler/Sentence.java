package com.daioware.language.compiler;

import com.daioware.language.compiler.Symbol.Type;

public class Sentence extends Node{
	
	public Node data;
	private String separator;
	
	public Sentence(Node data,Node next) {
		setNext(next);
		setData(data);
		setSeparator(";");
	}
	
	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public Node getData() {
		return data;
	}

	public void setData(Node data) {
		this.data = data;
	}
	public String toString(){
		StringBuilder info=new StringBuilder();
        Sentence next;
        info.append(data).append(getSeparator()).append("\n");
        next=this.getNext().toSentence();
        while(next!=null){
            info.append(next.toSentence().getData()).append(getSeparator()).append("\n");
            next=(Sentence)next.getNext();
        }
        return info.toString();
    }
	public Type checkType(){
		Type dataType=data.checkType();
        if(!dataType.equals(Type.ERROR)){
        	setType((getNext()!=null)?getNext().checkType():dataType);
            return getType();
        }
        else{
            setType(dataType);
            return getType();
        }
    }
    public String getCode(){
    	StringBuilder info=new StringBuilder();		
    	if(data!=null) info.append(data.getCode());
		if(next!=null) info.append(next.getCode());
        return info.toString();
	}
}
