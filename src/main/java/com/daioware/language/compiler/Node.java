package com.daioware.language.compiler;

import com.daioware.language.compiler.Symbol.Type;

public abstract class Node {
	protected String text;
	protected Node next;
	protected Type type;
	
	public Node() {
		
	}
	public Node(String text, Node next, Type type) {
		setText(text);
		setNext(next);
		setType(type);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	//Not sure if that is a good idea
	public Sentence toSentence() {
		return (Sentence)this;
	}
	public Assignment toAssignment() {
		return (Assignment)this;
	}
	public Expression toExpression() {
		return (Expression)this;
	}
	public LogicExpression toLogicExpression() {
		return (LogicExpression)this;
	}
	
	public abstract Type checkType();
	public abstract String getCode();
}
