package com.daioware.language.compiler;

public class Symbol {
	public static enum Type{
	    INTEGER,
	    FLOAT,
	    OPERATOR,
	    IDENTIFIER,
	    OPEN_PARENTHESIS,
	    CLOSE_PARENTHESIS,
	    SEMICOLON,
	    LOGICAL_OPERATOR,
	    STRING,
	    INCOMPLETE_STRING,
	    ASSIG_OPERATOR,
	    COMMA,
	    DOUBLE_POINT,
	    OPEN_BRACKETS, CLOSE_BRACKETS, CLOSE_BRACES, OPEN_BRACES,
	    WHITE_SPACE, ARITHMETICS_OPERATOR, 
	    ARITHMETICS_OPERATOR_COMB,
	    REM_COMBINATED, RELATIONAL_OPERATOR
	    ,UNKNOWN,
	    RESERVED_WORD
	    ,ERROR, QUOTES
	}
	private String text;
	private Type type;
	public Symbol(String text, Type type) {
		setText(text);
		setType(type);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public boolean isType(Type type) {
		return getType().equals(type);
	}
	@Override
	public String toString() {
		return "Symbol [text= " + text + ", type=" + type + "]";
	}
	
	
}
