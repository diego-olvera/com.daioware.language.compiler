package com.daioware.language.compiler;

public class Assignment extends Expression{

	
	public Assignment(Expression left, Expression right) {
		super(left, right);
		setOperator("=");
	}

	@Override
	public boolean evaluate(Evaluator ev) throws InvalidEvaluation {
		throw new InvalidEvaluation("Assignment can not be evaluated");
	}	
}
