package com.daioware.language.compiler;

public class LogicExpression extends Expression{

	public LogicExpression(Expression left, Expression right, String operator) {
		super(left, right, operator);
	}	
	public boolean evaluate(Evaluator ev) throws InvalidEvaluation{
		String operator=getOperator();
		Boolean leftValue,rightValue;
		boolean result;
		InvalidEvaluation leftEx,rightEx;
		switch(operator) {
		case "&&":
			//intelligent evaluation
			try{
				leftValue=left.evaluate(ev);
				leftEx=null;
			}catch(InvalidEvaluation e1) {
				leftValue=null;
				leftEx=e1;
			}
			try{
				rightValue=right.evaluate(ev);
				rightEx=null;
			}catch(InvalidEvaluation e2) {	
				rightValue=null;
				rightEx=e2;
			}
			if(leftValue!=null && rightValue!=null) {
				result=leftValue&&rightValue;
			}
			else if(leftValue!=null && rightValue==null && !leftValue) {
				result=false;
			}
			else if(rightValue!=null&& leftValue==null && !rightValue ) {
				result=false;
			}
			else {
				throw new InvalidEvaluation(leftEx!=null?leftEx.getMessage():""
						+(rightEx!=null?" "+rightEx.getMessage():""));
			}
			//dummy evaluation
			//result= left.evaluate(ev)&&right.evaluate(ev);
			break;
		case "||":
			//intelligent evaluation
			result=false;
			try{
				leftValue=left.evaluate(ev);
				leftEx=null;
				result=leftValue;
			}catch(InvalidEvaluation e1) {
				leftValue=null;
				leftEx=e1;
			}
			try{
				rightValue=right.evaluate(ev);
				rightEx=null;
				result=rightValue;
			}catch(InvalidEvaluation e2) {	
				rightValue=null;
				rightEx=e2;
			}
			if(leftValue==null && rightValue==null) {
				throw new InvalidEvaluation(leftEx!=null?leftEx.getMessage():""
						+(rightEx!=null?" "+rightEx.getMessage():""));
			}
			//dummy evaluation
			//result= left.evaluate(ev)||right.evaluate(ev);
			break;
		default:
			throw new InvalidEvaluation("Not valid operator:"+operator);
		}
		return result;
	}
}
