package com.daioware.language.compiler;

public class SyntacticException extends Exception{
	private static final long serialVersionUID = 1L;

	public SyntacticException(String message) {
		super(message);
	}

}
