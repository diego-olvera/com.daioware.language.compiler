package com.daioware.language.compiler.parser.cmd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.daioware.language.compiler.InvalidSymbolException;
import com.daioware.language.compiler.LexicalAnalyzer;
import com.daioware.language.compiler.Symbol;
import com.daioware.language.compiler.Symbol.Type;
import com.daioware.language.compiler.parser.GenericParser;
import com.daioware.language.compiler.parser.ParseException;

public class CommandArgumentsParser implements GenericParser<String[],Map<String,String>> {

	private String argumentCharacter;
	private String assignationCharacter;
	private List<String> flags;
	
	public CommandArgumentsParser() {
		this("-","=",new ArrayList<String>(0));
	}

	public CommandArgumentsParser(String argumentCharacter, String assignationCharacter,List<String> flags) {
		setArgumentCharacter(argumentCharacter);
		setAssignationCharacter(assignationCharacter);
		setFlags(flags);
	}

	public String getArgumentCharacter() {
		return argumentCharacter;
	}

	public String getAssignationCharacter() {
		return assignationCharacter;
	}

	public void setArgumentCharacter(String argumentCharacter) {
		this.argumentCharacter = argumentCharacter;
	}

	public void setAssignationCharacter(String assignationCharacter) {
		this.assignationCharacter = assignationCharacter;
	}

	public List<String> getFlags() {
		return flags;
	}

	public void setFlags(List<String> flags) {
		this.flags = flags;
	}

	@Override
	public Map<String, String> parse(String[] source) throws ParseException{
		StringBuilder text=new StringBuilder();
		Map<String,String> argsMap=new HashMap<String,String>();
		int argWithoutKeyCounter,state;
		String key=null,currentSymbol;
		List<String> flags=getFlags();
		String argChar=getArgumentCharacter(),assigChar=getAssignationCharacter();
		ListIterator<Symbol> symbolIterator;
		StringBuilder valueBuilder=new StringBuilder();
		Symbol symbolAux,symbol;
		for(String s:source) {
			text.append(s).append(" ");
		}
		try {
			symbolIterator=new LexicalAnalyzer(text.toString()).parse().listIterator();
			state=argWithoutKeyCounter=0;
			while(symbolIterator.hasNext()) {
				currentSymbol=(symbol=symbolIterator.next()).getText().trim();
				switch(state) {
				case 0:
					if(!symbol.isType(Type.STRING) && currentSymbol.startsWith(argChar)) {
						key=symbolIterator.next().getText();
						if(flags.contains(key)) {
							argsMap.put(key,"");
							state=0;
						}
						else {
							state=1;
						}
					}
					else if(!currentSymbol.equals("")){//value without key
						argsMap.put(String.valueOf(argWithoutKeyCounter++),currentSymbol);
					}
					break;
				case 1:
					if(!currentSymbol.equals(assigChar) && !currentSymbol.trim().equals("")) {
						state=0;
						valueBuilder=new StringBuilder(currentSymbol);
						//append until it finds a white space
						while(symbolIterator.hasNext()) {
							symbolAux=symbolIterator.next();
							if(!symbolAux.isType(Type.WHITE_SPACE)) {
								valueBuilder.append(symbolAux.getText());
							}
							else {
								symbolIterator.previous();
								break;
							}
						}
						argsMap.put(key,valueBuilder.toString());
					}
					break;
				}				
			}
			if(state==1) {//key without value
				argsMap.put(key,"");
			}
		} catch (InvalidSymbolException e) {
			throw new ParseException("Parse Exception", e);
		}
		return argsMap;
	}

}
