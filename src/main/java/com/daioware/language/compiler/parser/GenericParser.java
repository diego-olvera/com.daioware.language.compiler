package com.daioware.language.compiler.parser;

public interface GenericParser<Source,Transformation> {
	public abstract Transformation parse(Source source) throws ParseException;
}
