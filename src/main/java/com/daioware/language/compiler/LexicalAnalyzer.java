package com.daioware.language.compiler;

import static com.daioware.language.compiler.Symbol.Type.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.daioware.language.compiler.Symbol.Type;

public class LexicalAnalyzer {

	private static enum State{
		INIT(false),
		IDENTIFIER(true), 
		INTEGER(true),
		FLOAT(true),
		INCOMPLETE_STRING(true),
		STRING_WITH_BACKLASH(true),
		COMPLETE_STRING(true),
		RELATIONAL_OPERATOR(true), 
		LOGICAL_OPERATOR_AND(true),
		LOGICAL_OPERATOR_OR(true),
		VALID_STATE(true)
		;
		private boolean validFinalState;

		private State(boolean validFinalState) {
			this.validFinalState = validFinalState;
		}
		
		public boolean isValidFinalState() {
			return validFinalState;
		}
	}
	
	private State currentState;
	private StringBuilder currentText;
	private int currentIndex;
	private char ch;
	private String text;
	
	public LexicalAnalyzer(String text) {
		setText(text);
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public State getCurrentState() {
		return currentState;
	}

	public void setCurrentState(State currentState) {
		this.currentState = currentState;
		appendCurrentCharacter();
	}
	protected boolean isLastChar() {
		return currentIndex>=getTextSize();
	}
	
	public Symbol setCurrentStateAndAcceptIfIsFinalChar(State currentState,Type type) throws InvalidSymbolException {
		this.currentState = currentState;
		appendCurrentCharacter();
		if(isLastChar()) {
			return acceptCurrentSymbol(type);	
		}
		else {
			return null;
		}
	}
	
	protected void appendCurrentCharacter() {
		currentText.append(ch);
	}
	
	public StringBuilder getCurrentText() {
		return currentText;
	}

	public void setCurrentText(StringBuilder currentText) {
		this.currentText = currentText;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public int getTextSize() {
		return getText().length();
	}

	public char getCh() {
		return ch;
	}

	public void setCh(char ch) {
		this.ch = ch;
	}

	public List<Symbol> parse() throws InvalidSymbolException{
		return parse(new ArrayList<>());
	}
	
	protected boolean prevChar() {
		if(currentIndex>0) {
			currentIndex--;
			return true;
		}
		else {
			return false;
		}
	}
	
	protected boolean isValidState() {
		return currentState.isValidFinalState();
	}
	
	protected Symbol acceptCurrentSymbol(Type type) throws InvalidSymbolException {
		if(isValidState()) {
			return new Symbol(currentText.toString(),type);
		}
		else {
			throw new InvalidSymbolException(currentText.toString());
		}
	}
	protected Symbol acceptCurrentSymbol(State state,Type type) throws InvalidSymbolException {
		setCurrentState(state);
		return acceptCurrentSymbol(type);
	}
	
	public List<Symbol> parse(List<Symbol> symbols) throws InvalidSymbolException{
		int length;
		Symbol currentSymbol;
		length=text.length();
		setCurrentIndex(0);
		while(currentIndex<length && (currentSymbol=nextSymbol())!=null) {
			symbols.add(currentSymbol);
		}
		return symbols;
	}

	protected boolean nextChar() {
		int length=getTextSize();
		if(currentIndex>=0 && currentIndex<length) {
			ch=getText().charAt(currentIndex++);
			return true;
		}
		else {
			return false;
		}
	}
	
	public Symbol nextSymbol() throws InvalidSymbolException {
		Symbol symbol=null;
		String symbolText;
		setCurrentText(new StringBuilder());
		currentState=State.INIT;
		while(symbol==null && nextChar()) {
			//System.out.println("cState:"+currentState+", ch:"+ch);//log purposes, don't remove
			switch(currentState) {
				case INIT:
					if(Character.isWhitespace(ch))
						symbol=acceptCurrentSymbol(State.VALID_STATE,Type.WHITE_SPACE);
					else if(Character.isLetter(ch) || ch=='_') 
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.IDENTIFIER,Type.IDENTIFIER);
					else if(Character.isDigit(ch))	
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.INTEGER,Type.INTEGER);
					else if(ch=='"')
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.INCOMPLETE_STRING,Type.QUOTES);
					else if(ch=='>' || ch=='<')
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.RELATIONAL_OPERATOR,Type.RELATIONAL_OPERATOR);
					else if(ch=='&')
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.LOGICAL_OPERATOR_AND,Type.LOGICAL_OPERATOR);
					else if(ch=='|')
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.LOGICAL_OPERATOR_OR,Type.LOGICAL_OPERATOR);
					else if(ch=='^' || ch=='?')
						symbol=acceptCurrentSymbol(State.VALID_STATE,Type.LOGICAL_OPERATOR);
					else if(ch=='+' || ch=='-' || ch=='/' || ch=='*' || ch=='%' || ch=='=')
						symbol=acceptCurrentSymbol(State.VALID_STATE,Type.ARITHMETICS_OPERATOR);
					else 
						symbol=acceptCurrentSymbol(State.VALID_STATE,Type.UNKNOWN);
					break;
				case IDENTIFIER:
					if(Character.isLetter(ch) || ch=='_' || Character.isDigit(ch)) {
						symbol=setCurrentStateAndAcceptIfIsFinalChar(currentState,Type.IDENTIFIER);
					}
					else{
						prevChar();
						symbol=acceptCurrentSymbol(Type.IDENTIFIER);
					}
					break;
				case INTEGER:
					if(Character.isDigit(ch)) {
						symbol=setCurrentStateAndAcceptIfIsFinalChar(currentState,Type.INTEGER);
					}
					else if(ch=='.') {
						setCurrentState(State.FLOAT);
					}
					else {
						prevChar();
						symbol=acceptCurrentSymbol(Type.INTEGER);
					}
					break;
				case FLOAT:
					if(Character.isDigit(ch)) {
						symbol=setCurrentStateAndAcceptIfIsFinalChar(currentState,Type.FLOAT);
					}
					else {
						prevChar();
						symbol=acceptCurrentSymbol(Type.FLOAT);
					}
					break;
				case INCOMPLETE_STRING:
					if(ch=='\\'){//escaping character '\'
						symbol=setCurrentStateAndAcceptIfIsFinalChar(State.STRING_WITH_BACKLASH,Type.INCOMPLETE_STRING);
					}		
					else if(ch=='"') {
						symbol=acceptCurrentSymbol(STRING);
					}
					else {
						symbol=setCurrentStateAndAcceptIfIsFinalChar(currentState,Type.INCOMPLETE_STRING);
					}
					if(symbol!=null) {
						symbolText=symbol.getText();
						if(symbolText.length()>=1) {
							symbol.setText(symbolText.substring(1,symbolText.length()));
						}
					}
					break;
				case STRING_WITH_BACKLASH:
					if(ch=='"') {
						int currentTextSize=currentText.length();
						currentText.delete(currentTextSize-1, currentTextSize);
					}
					symbol=setCurrentStateAndAcceptIfIsFinalChar(State.INCOMPLETE_STRING,Type.INCOMPLETE_STRING);
					break;
				case RELATIONAL_OPERATOR:
					if(ch!='=') 
						prevChar();
					else
						appendCurrentCharacter();
					symbol=acceptCurrentSymbol(Type.RELATIONAL_OPERATOR);
					break;
				case LOGICAL_OPERATOR_AND:
					if(ch=='&')
						appendCurrentCharacter();
					else
						prevChar();
					symbol=acceptCurrentSymbol(Type.LOGICAL_OPERATOR);		
					break;
				case LOGICAL_OPERATOR_OR:
					if(ch=='|')
						appendCurrentCharacter();
					else
						prevChar();
					symbol=acceptCurrentSymbol(Type.LOGICAL_OPERATOR);		
					break;
				default:
			}
		}
		return symbol;
	}
	
	public void remove(List<Symbol> symbols,List<Type> typesToRemove) {
		Iterator<Symbol> it=symbols.iterator();
		while(it.hasNext()) {
			if(typesToRemove.contains(it.next().getType())) {
				it.remove();
			}
		}
	}
	
}
