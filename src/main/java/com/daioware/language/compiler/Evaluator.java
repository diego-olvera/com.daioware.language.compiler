package com.daioware.language.compiler;

public interface Evaluator {
	
	boolean isTrue(String name)throws InvalidEvaluation;
}
