package com.daioware.language.compiler;

import java.util.function.Consumer;

import com.daioware.language.compiler.Symbol.Type;

public abstract class Expression extends Node {
	
	protected Expression left;
	protected Expression right;
	protected String operator;
	
	public Expression() {
	}


	public Expression(Expression left, Expression right, String operator) {
		setLeft(left);
		setRight(right);
		setOperator(operator);
	}


	public Expression(Expression left, Expression right) {
		this(left,right,"");
	}

	public Expression getLeft() {
		return left;
	}


	public void setLeft(Expression left) {
		this.left = left;
	}


	public Expression getRight() {
		return right;
	}


	public void setRight(Expression right) {
		this.right = right;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	@Override
	public String getCode() {
		return toString();
	}
	public Type checkType() {
		return null;
	}
	public Expression getLowestLeft() {
		Expression aux=this;
		while(aux!=null) {
			if(aux.left==null)break;
			else aux=aux.left;
		}
		return aux;
	}
	public Expression getLowestRight() {
		Expression aux=this;
		while(aux!=null) {
			if(aux.right==null)break;
			else aux=aux.right;
		}
		return aux;
	}
	public abstract boolean evaluate(Evaluator ev) throws InvalidEvaluation;
	public void consumeVars(Consumer<Object> c) {
		if(left!=null){
            left.consumeVars(c);
        }
        if(right!=null){
            right.consumeVars(c);
        }
	}

	public String toString(){
        StringBuilder info=new StringBuilder();
        if(left!=null){
            info.append(left).append(" ");
        }
        if(right!=null){
            info.append(right).append(" ");
        }
        info.append(getOperator());
        return info.toString();
    }
	
}
