package com.daioware.language.compiler;

public class InvalidSymbolException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidSymbolException(String message) {
		super(message);
	}

	public InvalidSymbolException() {
		super();
	}

	public InvalidSymbolException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidSymbolException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidSymbolException(Throwable cause) {
		super(cause);
	}

}
