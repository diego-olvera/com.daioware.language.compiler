package com.daioware.language.compiler;

import com.daioware.language.compiler.Symbol.Type;

public class Number extends Expression{
	public Number(String text) {
		super();
		setText(text);
	}
	
	@Override
	public String getCode() {
		return toString();
	}
	public Type checkType() {
		return getType();
	}
	public String toString(){
        return getText();
    }

	@Override
	public boolean evaluate(Evaluator ev) throws InvalidEvaluation {
		return Double.parseDouble(getText())>0;
	}
}
