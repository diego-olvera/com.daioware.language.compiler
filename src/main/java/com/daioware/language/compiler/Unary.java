package com.daioware.language.compiler;

public class Unary extends Expression{
	
	public Unary(Expression left,String operator) {
		super(left,null,operator);
	}
	public boolean evaluate(Evaluator ev) throws InvalidEvaluation{
		String operator=getOperator();
		switch(operator) {
			case "!":default:return !left.evaluate(ev);
		}
	}
	public String toString(){
        StringBuilder info=new StringBuilder();
        if(left!=null){
            info.append(left).append(" ");
        }
        info.append(getOperator());
        return info.toString();
    }
}
